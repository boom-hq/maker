// The MIT License (MIT)
//
// Copyright (c) 2018 Cranky Kernel
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package types

type PriceSource string

const (
	PriceSourceLast    PriceSource = "LAST_PRICE"
	PriceSourceBestBid PriceSource = "BEST_BID"
	PriceSourceBestAsk PriceSource = "BEST_ASK"
	PriceSourceManual  PriceSource = "MANUAL"
)

type LimitSellType string

const (
	LimitSellTypePercent LimitSellType = "PERCENT"
	LimitSellTypePrice   LimitSellType = "PRICE"
)

type TradeStatus string

const (
	TradeStatusNew         TradeStatus = "NEW"
	TradeStatusFailed      TradeStatus = "FAILED"
	TradeStatusPendingBuy  TradeStatus = "PENDING_BUY"
	TradeStatusWatching    TradeStatus = "WATCHING"
	TradeStatusPendingSell TradeStatus = "PENDING_SELL"
	TradeStatusDone        TradeStatus = "DONE"
	TradeStatusCanceled    TradeStatus = "CANCELED"
	TradeStatusAbandoned   TradeStatus = "ABANDONED"
)
