// The MIT License (MIT)
//
// Copyright (c) 2018 Cranky Kernel
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package types

func TradeStateV0ToTradeStateV1(old TradeStateV0) TradeState {
	state := TradeState{}
	state.Version = TRADE_STATE_VERSION
	state.TradeID = old.LocalID
	state.Symbol = old.Symbol
	state.OpenTime = old.OpenTime
	state.CloseTime = old.CloseTime
	state.Status = old.Status
	state.Fee = old.Fee
	state.BuyOrderId = old.BuyOrderId
	state.ClientOrderIDs = old.ClientOrderIDs
	state.BuyOrder = old.BuyOrder
	state.BuySideFills = old.BuySideFills
	state.BuyFillQuantity = old.BuyFillQuantity
	state.AverageBuyPrice = old.AverageBuyPrice
	state.BuyCost = old.BuyCost
	state.EffectiveBuyPrice = old.EffectiveBuyPrice
	state.SellOrderId = old.SellOrderId
	state.SellSideFills = old.SellSideFills
	state.SellFillQuantity = old.SellFillQuantity
	state.AverageSellPrice = old.AverageSellPrice
	state.SellCost = old.SellCost
	state.StopLoss = old.StopLoss
	state.LimitSell.Enabled = old.LimitSell.Enabled
	state.LimitSell.Type = LimitSellTypePercent
	state.LimitSell.Percent = old.LimitSell.Percent
	state.TrailingProfit = old.TrailingProfit
	state.Profit = old.Profit
	state.ProfitPercent = old.ProfitPercent
	state.LastBuyStatus = old.LastBuyStatus
	state.SellOrder = old.SellOrder
	state.LastPrice = old.LastPrice
	return state
}
