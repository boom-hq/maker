// The MIT License (MIT)
//
// Copyright (c) 2018 Cranky Kernel
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";

const SESSION_HEADER = "X-Session-ID";

/**
 * The MakerApiService contains methods that wrap around calls to the Maker
 * API.
 */
@Injectable({
    providedIn: "root"
})
export class MakerApiService {

    private sessionId: string = null;

    constructor(private http: HttpClient) {
    }

    post(url: string, body: any, options: any = null): Observable<any> {
        if (!options) {
            options = {};
        }
        let headers = options.headers || new HttpHeaders();
        if (this.sessionId) {
            headers = headers.set(SESSION_HEADER, this.sessionId);
        }
        options.headers = headers;
        return this.http.post(url, body, options);
    }

    get(url: string, options: any = null): Observable<any> {
        if (!options) {
            options = {};
        }
        let headers = options.headers || new HttpHeaders();
        if (this.sessionId) {
            headers = headers.set(SESSION_HEADER, this.sessionId);
        }
        options.headers = headers;
        return this.http.get(url, options);
    }

    delete(url: string, options: any = null): Observable<any> {
        if (!options) {
            options = {};
        }
        let headers = options.headers || new HttpHeaders();
        if (this.sessionId) {
            headers = headers.set(SESSION_HEADER, this.sessionId);
        }
        options.headers = headers;
        return this.http.delete(url, options);
    }

    setSessionId(sessionId: string) {
        console.log(`Session ID set to ${sessionId}`);
        this.sessionId = sessionId;
    }

    getConfig(): Observable<any> {
        return this.get("/api/config")
            .pipe(map((response) => {
                return flattenJson(response);
            }));
    }

    savePreferences(prefs: any): Observable<boolean> {
        return this.post("/api/config/preferences", prefs)
            .pipe(map(() => {
                return true;
            }));
    }

    login(username: string, password: string): Observable<any> {
        return this.http.post("/api/login", {
            username: username,
            password: password,
        });
    }

    getVersion(): Observable<any> {
        return this.get("/api/version");
    }

    openWebsocket(): WebSocket {
        let proto = window.location.protocol == "https:" ? "wss" : "ws";
        let url = `${proto}://${window.location.host}/ws?`;
        if (this.sessionId) {
            url = `${url}&sessionId=${this.sessionId}`;
        }
        console.log(`Opening websocket: ${url}`);
        return new WebSocket(url);
    }
}

function flattenJson(input) {
    const output = {};

    for (const i in input) {
        if (!input.hasOwnProperty(i)) {
            continue;
        }

        if ((typeof input[i]) === "object") {
            const flatObject = flattenJson(input[i]);
            for (const x in flatObject) {
                if (!flatObject.hasOwnProperty(x)) {
                    continue;
                }

                output[i + "." + x] = flatObject[x];
            }
        } else {
            output[i] = input[i];
        }
    }

    return output;
}
