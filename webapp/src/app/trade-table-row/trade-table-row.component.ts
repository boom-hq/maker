// The MIT License (MIT)
//
// Copyright (c) 2018 Cranky Kernel
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import {AfterViewInit, Component, Input, OnDestroy, OnInit} from "@angular/core";
import {MakerService, TradeStatus} from "../maker.service";
import {Logger, LoggerService} from "../logger.service";
import {ToastrService} from "../toastr.service";
import {AppTradeState} from '../trade-table/trade-table.component';
import * as $ from "jquery";
import {BinanceApiService} from "../binance-api.service";

interface SellAtPriceModel {
    price: number;
}

interface SellAtPercentModel {
    percent: number;
}

const sellAtPercentModels: {
    [key: string]: SellAtPercentModel
} = {};

function getSellAtPercentModel(tradeId: string): SellAtPercentModel {
    if (sellAtPercentModels.hasOwnProperty(tradeId)) {
        return sellAtPercentModels[tradeId];
    }
    const model: SellAtPercentModel = {
        percent: 0,
    };
    sellAtPercentModels[tradeId] = model;
    return sellAtPercentModels[tradeId];
}

const sellAtPriceModels: {
    [key: string]: SellAtPriceModel
} = {};

function getSellAtPriceModel(tradeId: string, defaults: number[]): SellAtPriceModel {

    let price = 0;

    for (let defaultPrice of defaults) {
        if (defaultPrice > 0) {
            price = defaultPrice;
            break;
        }
    }

    if (sellAtPriceModels.hasOwnProperty(tradeId)) {
        const model = sellAtPriceModels[tradeId];
        if (!model.price) {
            model.price = price;
        }
        return model;
    }
    const model: SellAtPriceModel = {
        price: price,
    };
    sellAtPriceModels[tradeId] = model;
    return sellAtPriceModels[tradeId];
}

enum MARKET_SELL_STATE {
    INITIAL = "INITIAL",
    CONFIRM = "CONFIRM",
}

@Component({
    selector: "[app-trade-table-row]",
    templateUrl: "./trade-table-row.component.html",
    styleUrls: ["./trade-table-row.component.scss"]
})
export class TradeTableRowComponent implements OnInit, OnDestroy, AfterViewInit {

    MARKET_SELL_STATE = MARKET_SELL_STATE;

    TradeStatus = TradeStatus;

    logger: Logger = null;

    @Input("trade") trade: AppTradeState = null;

    @Input() showArchiveButtons: boolean = true;

    @Input() showTradeButtons: boolean = false;

    sellAtPriceModel: SellAtPriceModel = null;
    sellAtPercentModel: SellAtPercentModel = null;

    marketSellState = MARKET_SELL_STATE.INITIAL;

    abandonState = 0;

    destroyHooks: any[] = [];

    constructor(public maker: MakerService,
                private toastr: ToastrService,
                private binanceApi: BinanceApiService,
                logger: LoggerService) {
        this.logger = logger.getLogger("TradeTableRowComponent");
    }

    ngOnInit() {
        this.sellAtPercentModel = getSellAtPercentModel(this.trade.TradeID);
        this.sellAtPriceModel = getSellAtPriceModel(this.trade.TradeID, [
            this.trade.EffectiveBuyPrice,
            +(this.trade.BuyOrder.Price * (1 + 0.001)).toFixed(8),
        ]);
    }

    ngOnDestroy() {
        for (const hook of this.destroyHooks) {
            hook();
        }
    }

    ngAfterViewInit() {
        let sellDropdownHandler = $("#sellDropdown-" + this.trade.TradeID).on("hidden.bs.dropdown", () => {
            // Reset market sell confirmation state.
            this.marketSellState = MARKET_SELL_STATE.INITIAL;

            // Reset abandon state.
            this.abandonState = 0;
        });
        this.destroyHooks.push(() => {
            sellDropdownHandler.off();
        });
    }

    cancelBuy() {
        this.maker.cancelBuy(this.trade.TradeID).subscribe(() => {
        }, (error) => {
            console.log("Failed to cancel buy order: " + JSON.stringify(error));
        });
    }

    cancelSell() {
        this.maker.cancelSell(this.trade).subscribe(() => {
        }, (error) => {
            this.toastr.error(`Failed to cancel sell order: ${error.error.message}`);
        });
    }

    limitSellAtPercent() {
        this.maker.limitSellByPercent(this.trade, +this.sellAtPercentModel.percent);
    }

    limitSellAtPrice() {
        this.maker.limitSellByPrice(this.trade, +this.sellAtPriceModel.price);
    }

    onMarketSellClick($event: any) {
        if (this.marketSellState == MARKET_SELL_STATE.INITIAL) {
            this.marketSellState = MARKET_SELL_STATE.CONFIRM;
            $event.stopPropagation();
        } else {
            this.maker.marketSell(this.trade);
        }
    }

    archive() {
        this.maker.archiveTrade(this.trade);
    }

    abandon() {
        this.maker.abandonTrade(this.trade);
    }

}
