// The MIT License (MIT)
//
// Copyright (c) 2018 Cranky Kernel
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import {Component, OnInit} from '@angular/core';
import {GIT_BRANCH, VERSION} from "../../environments/version";
import {HttpClient, HttpParams} from "@angular/common/http";
import {MakerService} from "../maker.service";
import * as $ from "jquery";

@Component({
    selector: 'app-about',
    templateUrl: './about.component.html',
    styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

    VERSION = VERSION;

    updateAvailable: boolean | null = null;

    releaseBranch = GIT_BRANCH;

    constructor(private http: HttpClient, private maker: MakerService) {
    }

    ngOnInit() {
        $("#aboutModal").on("show.bs.modal", () => {
            this.updateAvailable = null;
        });
    }

    checkVersion() {
        this.maker.getVersion().subscribe((myVersion) => {
            let params = new HttpParams()
                .set("version", this.VERSION)
                .set("opsys", myVersion.opsys)
                .set("arch", myVersion.arch);
            this.http.get("https://maker.crankykernel.com/files/versions.json", {
                params: params,
            }).subscribe((latestVersion) => {
                this.compareVersions(myVersion, latestVersion);
            });
        });
    }

    compareVersions(myVersion, latestVersion) {
        if (myVersion.git_branch == "master") {
            const channel = "development";
            try {
                const commit_id = latestVersion[channel][myVersion.opsys][myVersion.arch].commit_id;
                if (myVersion.git_revision != commit_id) {
                    console.log("Development release has been updated.");
                    this.updateAvailable = true;
                } else {
                    this.updateAvailable = false;
                }
            } catch (err) {
                console.log("Failed to check if development release channel has update: " + err);
                console.log(`- myVersion: ${JSON.stringify(myVersion)}`);
                console.log(`- latestVersion: ${JSON.stringify(latestVersion)}`);
            }
            return;
        }

        // Check release channel.
        try {
            const version = latestVersion["release"][myVersion.opsys][myVersion.arch].version;
            if (version != myVersion.version) {
                console.log("Release version has been updated.");
                this.updateAvailable = true;
            } else {
                this.updateAvailable = false;
            }
        } catch (err) {
            console.log("Failed to check if release channel has update: " + err);
            console.log(`- myVersion: ${JSON.stringify(myVersion)}`);
            console.log(`- latestVersion: ${JSON.stringify(latestVersion)}`);
        }

    }

}
