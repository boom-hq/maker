// The MIT License (MIT)
//
// Copyright (c) 2018 Cranky Kernel
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import {Component, OnInit} from "@angular/core";
import {HttpParams} from "@angular/common/http";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MakerApiService} from "../maker-api.service";
import {ToastrService} from "../toastr.service";
import {Router} from "@angular/router";
import {
    Config,
    ConfigKey,
    ConfigService,
    DEFAULT_BALANCE_PERCENTS,
    DEFAULT_BINANCE_BNB_WARNING_LEVEL
} from "../config.service";

@Component({
    selector: "app-config",
    templateUrl: "./config.component.html",
    styleUrls: ["./config.component.scss"]
})
export class ConfigComponent implements OnInit {

    config: Config = <Config>{};

    form: FormGroup = null;

    preferenceForm: FormGroup = null;

    testOk = false;

    apiKeyInputType = "password";

    showApiInput: boolean = false;

    constructor(private makerApi: MakerApiService,
                private toastr: ToastrService,
                private router: Router,
                private configService: ConfigService,
                private fb: FormBuilder) {
        this.toggleApiInputVisibility();
    }

    ngOnInit() {
        this.configService.loadConfig().subscribe(() => {
            this.config = this.configService.config;
            this.buildBinanceForm();
            this.buildPreferenceForm();
        });
    }

    private buildBinanceForm() {
        this.form = this.fb.group({
            binanceApiKey: [this.config["binance.api.key"],
                Validators.required],
            binanceApiSecret: [this.config["binance.api.secret"],
                Validators.required],
        });
    }

    private buildPreferenceForm() {
        let percents: string = null;
        if (this.configService.config[ConfigKey.PREFERENCE_BALANCE_PERCENTS]) {
            percents = this.configService.config[ConfigKey.PREFERENCE_BALANCE_PERCENTS];
        } else {
            percents = DEFAULT_BALANCE_PERCENTS;
        }
        let binanceBnbWarningLevel: number = DEFAULT_BINANCE_BNB_WARNING_LEVEL;
        if (this.configService.config[ConfigKey.PREFERENCE_BINANCE_BNB_WARNING_LEVEL]) {
            binanceBnbWarningLevel = this.configService.config[ConfigKey.PREFERENCE_BINANCE_BNB_WARNING_LEVEL];
        }
        this.preferenceForm = this.fb.group({
            balancePercents: [percents, Validators.pattern(/^[\d\., ]+$/)],
            binanceBnbWarningLevel: [binanceBnbWarningLevel],
        });
    }

    resetBinanceForm() {
        this.buildBinanceForm();
    }

    toggleApiInputVisibility() {
        if (this.showApiInput) {
            this.apiKeyInputType = "text";
        } else {
            this.apiKeyInputType = "password";
        }
    }

    binanceTest() {
        const formModel = this.form.value;

        const params = new HttpParams()
            .set("binance.api.key", formModel.binanceApiKey)
            .set("binance.api.secret", formModel.binanceApiSecret);
        this.makerApi.get("/api/binance/account/test", {
            params: params,
        }).subscribe((response) => {
            if (!response.ok) {
                this.toastr.error("Authentication test failed.");
                this.testOk = false;
            } else {
                this.toastr.info("Authentication OK.");
                this.testOk = true;
            }
        });
    }

    binanceSave() {
        const formModel: BinanceForm = <BinanceForm>this.form.value;
        this.configService.set(ConfigKey.BINANCE_API_KEY, formModel.binanceApiKey);
        this.configService.set(ConfigKey.BINANCE_API_SECRET, formModel.binanceApiSecret);
        this.configService.saveBinanceConfig().subscribe(() => {
            (<any>window).location = "/";
        }, (error) => {
            this.toastr.error(`Failed to save configuration: ${JSON.stringify(error)}.`);
        });
    }

    savePreferences() {
        const prefs: PreferenceForm = <PreferenceForm>this.preferenceForm.value;
        this.configService.set(ConfigKey.PREFERENCE_BALANCE_PERCENTS, prefs.balancePercents);
        this.configService.set(ConfigKey.PREFERENCE_BINANCE_BNB_WARNING_LEVEL, prefs.binanceBnbWarningLevel);
        this.configService.savePreferences().subscribe(() => {
            this.toastr.success("Preferences saved.");
        }, (error) => {
            this.toastr.error(`Failed to save preferences: ${JSON.stringify(error)}`);
        });
    }

    resetPreferences() {
        this.buildPreferenceForm();
    }

}

interface BinanceForm {
    binanceApiKey: string;
    binanceApiSecret: string;
}

interface PreferenceForm {
    balancePercents: string;
    binanceBnbWarningLevel: number;
}
