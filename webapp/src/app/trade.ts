// The MIT License (MIT)
//
// Copyright (c) 2018 Cranky Kernel
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import {AppTradeState} from './trade-table/trade-table.component';
import {TradeState, TradeStatus} from './maker.service';

export class Trade {

    static onLastPrice(trade: AppTradeState, lastPrice: number) {
        trade.LastPrice = lastPrice;
        Trade.updateProfit(trade, lastPrice);
    }

    static updateProfit(trade: AppTradeState, lastPrice: number) {
        if (trade.BuyFillQuantity > 0) {
            const grossSellCost = lastPrice * trade.SellableQuantity;
            const netSellCost = grossSellCost * (1 - trade.Fee);
            trade.ProfitPercent = (netSellCost - trade.BuyCost) / trade.BuyCost * 100;
        }

        // Calculate the percent difference between our buy price and the
        // current price.
        const diffFromBuyPrice = trade.BuyOrder.Price - lastPrice;
        trade.buyPercentOffsetPercent = diffFromBuyPrice / lastPrice * 100;
    }

    static isOpen(trade: TradeState): boolean {
        switch (trade.Status) {
            case TradeStatus.DONE:
            case TradeStatus.FAILED:
            case TradeStatus.CANCELED:
            case TradeStatus.ABANDONED:
                return false;
            default:
                return true;
        }
    }

}

