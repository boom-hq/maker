// The MIT License (MIT)
//
// Copyright (c) 2018 Cranky Kernel
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import {Injectable} from '@angular/core';
import {HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {MakerApiService} from "./maker-api.service";
import {map} from "rxjs/operators";
import {
    BinanceAccountInfo,
    BinanceRestAccountInfoResrponse,
    ExchangeInfo,
    RestExchangeInfoResponse
} from "./binance-api.service";

@Injectable({
    providedIn: 'root'
})
export class BinanceProxyService {

    constructor(private makerApi: MakerApiService) {
    }

    private get(path: string, params: HttpParams = new HttpParams()): Observable<Object> {
        const url = `${path}`;
        return this.makerApi.get(url, {
            params: params,
        });
    }

    getTicker24h(symbol: string): Observable<Ticker24hResponse> {
        const endpoint = "/proxy/binance/api/v1/ticker/24hr";
        const params = new HttpParams().set("symbol", symbol);
        return <Observable<Ticker24hResponse>>this.get(endpoint, params);
    }

    getAccountInfo(): Observable<BinanceAccountInfo> {
        return this.makerApi.get("/api/binance/proxy/getAccount")
            .pipe(map((restResponse: BinanceRestAccountInfoResrponse) => {
                const accountInfo = BinanceAccountInfo.fromRest(restResponse);
                return accountInfo;
            }));
    }

    getExchangeInfo(): Observable<ExchangeInfo> {
        const endpoint = "/proxy/binance/api/v1/exchangeInfo";
        return this.get(endpoint, null).pipe(map((info: RestExchangeInfoResponse) => {
            return ExchangeInfo.fromRest(info);
        }));
    }

}

export interface Ticker24hResponse {
    symbol: string;
    priceChangePercent: string;
    lastPrice: string;
    bidPrice: string;
    askPrice: string;
    quoteVolume: string;
}
